import React, { Component } from 'react';
import {StyleSheet,Text, View } from 'react-native';

type HeaderPage = {
    title: string
}

const Header = ({title} : HeaderPage) => {
    return(
        <View style={styles.header}>
            <Text style={styles.text}>{title}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
      flex: 1,
      height : '100',
      backgroundColor : 'darkslateblue'
    },

    text: {
      color: '#fff',
      fontSize: 23,
      fontWeight: 'bold',
      textAlign : 'center'
    }
});

export default Header;
